```
npm install
```

```
npm start
```

Run the following query and open the tracing tab on the bottom right.

```
query {
  books {
    title
    author {
      name
    }
  }
}
```
